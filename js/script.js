function FuncionarAxio() {
    const url = "/html/alumnos.json"
    const general = document.getElementById("general")
    var AlumProm
    var GeneralP = 0

    axios
        .get(url)
        .then((res) => {
            Tablas(res.data)
        })
        .catch((error) => {
            console.log("Hubo un error en la solicitud" + error)
        })

    function Tablas(data) {
        const res = document.getElementById('contacto')
        res.innerHTML = ""
        for (item of data) {
            AlumProm = ((item.geografia + item.fisica + item.quimica + item.matematicas) / 4 )
            GeneralP = GeneralP + AlumProm 
            res.innerHTML += `
                <tr>
					<td>${item.id}</td>
					<td>${item.matricula}</td>
					<td>${item.nombre}</td>
					<td>${item.matematicas}</td>
                    <td>${item.quimica}</td>
					<td>${item.fisica}</td>
					<td>${item.geografia}</td>
					<td>${AlumProm}</td>
				</tr>`;
            
        }

        general.innerHTML = `${GeneralP / 18}`
    }
}

document.getElementById("btnMostrar").addEventListener("click", function () {
    FuncionarAxio();
})

document.getElementById("btnLimpiar").addEventListener("click", function () {
    const res = document.getElementById("pet")
    res.innerHTML = `
    <table id="pet" class="table table-hover table-dark">
    <thead>
        <th>Id </th>
        <th>Matricula </th>
        <th>Nombre</th>
        <th>Matematicas</th>
        <th>Quimica</th>
        <th>Fisica</th>
        <th>Geografia</th>
        <th>Promedio</th>
    </thead>
    <tbody id="contacto">
    </tbody>
    </table>`
    general.innerHTML=""

})
